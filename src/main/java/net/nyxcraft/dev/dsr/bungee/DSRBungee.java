package net.nyxcraft.dev.dsr.bungee;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.plugin.Plugin;
import net.nyxcraft.dev.dsr.bungee.commands.MOTDCommands;
import net.nyxcraft.dev.dsr.bungee.database.ProxyDatabaseManager;
import net.nyxcraft.dev.dsr.bungee.database.entities.ProxySettings;
import net.nyxcraft.dev.dsr.bungee.server.DynamicRegistrationModule;
import net.nyxcraft.dev.dsr.bungee.server.PingListener;
import net.nyxcraft.dev.dsr.redis.RedisHandler;
import net.nyxcraft.dev.dsr.utils.JsonConfig;

import java.io.File;

public class DSRBungee extends Plugin {

    private static Gson gson = new GsonBuilder().create();
    private static DSRBungee instance;

    private DynamicSettings dynamicSettings;
    private ProxyDatabaseManager databaseManager;
    private ProxySettings proxySettings;
    private RedisHandler<Plugin> redis;
    private DynamicRegistrationModule dynamicRegistrationModule;

    public void onEnable() {
        instance = this;
        ProxyServer.getInstance().getPluginManager().registerListener(this, new PingListener());

        loadConfig();
        databaseManager = new ProxyDatabaseManager();
        fetchProxySettings();
        redis = init("redis.json");
        dynamicRegistrationModule = new DynamicRegistrationModule(this);
        registerComamnds();
    }

    private RedisHandler init(String file) {
        return new RedisHandler<>(getLogger(), dynamicSettings.getRedis(), this, ProxyServer.getInstance().getScheduler()::runAsync);
    }

    public void onDisable() {
        redis.disable();
    }

    private DynamicSettings loadConfig() {
        if (!getDataFolder().exists()) {
            getLogger().info("Config folder not found! Creating...");
            getDataFolder().mkdir();
        }

        File file = new File(getDataFolder(), "settings.json");
        dynamicSettings = JsonConfig.load(file, DynamicSettings.class);

        if (!file.exists()) {
            dynamicSettings.save(file);
        }

        return dynamicSettings;
    }

    public void fetchProxySettings() {
        proxySettings = ProxyDataAPI.loadSettings();
    }

    public RedisHandler getRedis() {
        return redis;
    }

    public DynamicRegistrationModule getDynamicRegistrationModule() {
        return dynamicRegistrationModule;
    }

    public static DSRBungee getInstance() {
        return instance;
    }

    public ProxySettings getProxySettings() {
        return proxySettings;
    }

    public DynamicSettings getDynamicSettings() {
        return dynamicSettings;
    }

    public void registerComamnds() {
        ProxyServer.getInstance().getPluginManager().registerCommand(this, new MOTDCommands(this));
    }
}
