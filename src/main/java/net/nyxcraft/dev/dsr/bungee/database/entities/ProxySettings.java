package net.nyxcraft.dev.dsr.bungee.database.entities;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

@Entity(value = "network_proxy_settings", noClassnameStored = true)
public class ProxySettings {

    @Id
    public ObjectId id;
    public String motdLineOne = "&f▁ &5▂ &7▃ &5▄ &f▅ &5▆ &7█&5&l NyxCraft &r&7█ &5▆ &f▅ &5▄ &7▃ &5▂ &f▁";
    public String motdLineTwo = "";

}
