package net.nyxcraft.dev.dsr.bungee.server;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.ServerPing;
import net.md_5.bungee.api.event.ProxyPingEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;
import net.md_5.bungee.event.EventPriority;
import net.nyxcraft.dev.dsr.bungee.DSRBungee;
import net.nyxcraft.dev.dsr.bungee.database.entities.ProxySettings;

public class PingListener implements Listener {

    @EventHandler(priority = EventPriority.NORMAL)
    public void onPing(ProxyPingEvent event) {
        ProxySettings settings = DSRBungee.getInstance().getProxySettings();
        String motd = "&f▁ &5▂ &7▃ &5▄ &f▅ &5▆ &7█&5&l NyxCraft &r&7█ &5▆ &f▅ &5▄ &7▃ &5▂ &f▁";
        if (settings != null) {
            if (settings.motdLineOne != null) {
                motd = settings.motdLineOne;
                if (settings.motdLineTwo != null) {
                    motd += "\n" + settings.motdLineTwo;
                }
            }
        }

        event.getResponse().getPlayers().setOnline(ServerHeartbeatHandler.getPlayersOnline());
        event.setResponse(new ServerPing(event.getResponse().getVersion(),
                event.getResponse().getPlayers(),
                ChatColor.translateAlternateColorCodes('&', motd),
                event.getResponse().getFaviconObject()));
    }

}
