package net.nyxcraft.dev.dsr.bungee;

import net.nyxcraft.dev.dsr.bungee.database.dao.ProxySettingsDAO;
import net.nyxcraft.dev.dsr.bungee.database.entities.ProxySettings;

public class ProxyDataAPI {

    public static ProxySettings loadSettings() {
        ProxySettings settings = ProxySettingsDAO.getInstance().find().get();
        if (settings == null) {
            settings = new ProxySettings();
            ProxySettingsDAO.getInstance().save(settings);
        }
        return settings;
    }

}
