package net.nyxcraft.dev.dsr.bungee.database.dao;

import net.nyxcraft.dev.dsr.bungee.database.entities.ProxySettings;
import org.bson.types.ObjectId;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.dao.BasicDAO;

public class ProxySettingsDAO extends BasicDAO<ProxySettings, ObjectId> {

    private static ProxySettingsDAO instance;

    public ProxySettingsDAO(Datastore ds) {
        super(ds);
        instance = this;
    }

    public static ProxySettingsDAO getInstance() {
        return instance;
    }

}
