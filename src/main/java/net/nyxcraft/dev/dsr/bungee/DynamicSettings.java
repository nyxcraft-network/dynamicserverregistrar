package net.nyxcraft.dev.dsr.bungee;

import net.nyxcraft.dev.dsr.redis.RediSettings;
import net.nyxcraft.dev.dsr.utils.JsonConfig;

public class DynamicSettings extends JsonConfig {

    private RediSettings redis = new RediSettings("localhost", 6379, null, -1);

    public RediSettings getRedis() {
        return redis;
    }
}
