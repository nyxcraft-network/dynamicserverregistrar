package net.nyxcraft.dev.dsr.bungee.commands;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.plugin.Command;
import net.nyxcraft.dev.dsr.bungee.DSRBungee;
import net.nyxcraft.dev.dsr.bungee.ProxyDataAPI;
import net.nyxcraft.dev.dsr.bungee.database.dao.ProxySettingsDAO;
import net.nyxcraft.dev.dsr.bungee.database.entities.ProxySettings;
import net.nyxcraft.dev.dsr.redis.RedisHandler;
import net.nyxcraft.dev.dsr.redis.pubsub.NetTask;
import net.nyxcraft.dev.dsr.redis.pubsub.NetTaskSubscribe;

import java.util.HashMap;
import java.util.concurrent.TimeUnit;

public class MOTDCommands extends Command {
    private RedisHandler handler;
    private ProxySettings settings;

    public MOTDCommands(DSRBungee plugin) {
        super("motd");
        this.handler = plugin.getRedis();
        this.settings = plugin.getProxySettings();

        handler.registerChannel("motd");
        handler.registerTask(this);
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if (args.length == 0) {
            sender.sendMessage(ChatColor.GOLD + "USAGE: " + ChatColor.RED + "/motd <set|clear|reset>");
            return;
        }

        switch (args[0].toLowerCase()) {
            case "set":
                setMOTD(sender, args);
                return;
            case "clear":
                clear(sender);
                return;
            case "reset":
                reset(sender);
                return;
        }
    }

    public void setMOTD(CommandSender sender, String[] args) {
        if (args.length < 3) {
            sender.sendMessage(ChatColor.GOLD + "USAGE: " + ChatColor.RED + "/motd set <line #: 1|2> <motd>");
            return;
        }

        int line;
        try {
            line = Integer.parseInt(args[1]);
        } catch (NumberFormatException e) {
            line = 1;
        }

        String motd = concat(2, args);

        if (line == 1) {
            settings.motdLineOne = motd;
        } else if (line == 2) {
            settings.motdLineTwo = motd;
        } else {
            return;
        }

        update();
        ProxySettingsDAO.getInstance().save(settings);
        sender.sendMessage(ChatColor.GREEN + "You have updated the network's MOTD!");
    }

    public String concat(int from, String[] args) {
        String string = null;
        for (int i = from; i < args.length; i++) {
            if (string == null) {
                string = args[i];
                continue;
            }

            string += " " + args[i];
        }

        return string;
    }

    public void clear(CommandSender sender) {
        settings.motdLineOne = "";
        settings.motdLineTwo = "";

        update();
        ProxySettingsDAO.getInstance().save(settings);
        sender.sendMessage(ChatColor.GREEN + "You have cleared the network's MOTD!");
    }

    public void reset(CommandSender sender) {
        settings.motdLineOne = null;
        settings.motdLineTwo = null;

        update();
        ProxySettingsDAO.getInstance().save(settings);
        sender.sendMessage(ChatColor.GREEN + "You have reset the network's MOTD!");
    }

    public void update() {
        NetTask.withName("motd").send("motd", handler);
    }

    @NetTaskSubscribe(name = "motd", args = {})
    public void update(HashMap<String, Object> data) {
        ProxyServer.getInstance().getScheduler().schedule(DSRBungee.getInstance(), () -> DSRBungee.getInstance().fetchProxySettings(), 5, TimeUnit.SECONDS);
    }
}
