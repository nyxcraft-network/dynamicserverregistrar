package net.nyxcraft.dev.dsr.bungee.database;

import net.nyxcraft.dev.dsr.bungee.database.dao.ProxySettingsDAO;
import net.nyxcraft.dev.ndb.NyxDB;
import net.nyxcraft.dev.ndb.mongodb.ResourceManager;
import org.mongodb.morphia.Datastore;

public class ProxyDatabaseManager {

    private ResourceManager resourceManager;
    private Datastore datastore;

    public ProxyDatabaseManager() {
        resourceManager = NyxDB.getResourceManager();
        datastore = resourceManager.getDatastore("net.nyxcraft.dev.prison.database.entities");
        init();
    }

    private void init() {
        datastore.ensureCaps();
        datastore.ensureIndexes();

        registerDAOs();
    }

    private void registerDAOs() {
        new ProxySettingsDAO(datastore);
    }

}
